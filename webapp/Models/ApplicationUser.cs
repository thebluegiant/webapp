﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Blitzmove.Models
{
    public class ApplicationUser : IdentityUser
    {
        [PersonalData]
        public string Name { get; set; }

        [PersonalData]
        public string JobTitle { get; set; }

        [PersonalData]
        public int Salary { get; set; }

        [PersonalData]
        public int CreditScore { get; set; }

        [PersonalData]
        public string ProfilePicture { get; set; }
    }
}
