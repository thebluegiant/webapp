using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;



namespace Blitzmove.Models
{
    public class Favorite
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid PlaceId { get; set; }
    }

    public class UserFavorites
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid UserId { get; set; }
        public IList<Favorite> PlaceIds { get; set; } = new List<Favorite>();
    }
}
    