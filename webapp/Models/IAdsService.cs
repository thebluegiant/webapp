﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blitzmove.Models
{
    public interface IAdsService
    {
        Task DeleteAsync(Guid id);
        Task UpdateAsync(Place place);                              
        Place Find(Guid id);
        Task<Place> FindAsync(Guid id);
        Task<Place[]> GetPlacesWithinAreaAsync(double ne_lng, double ne_lat, double sw_lng, double sw_lat);
        IQueryable<Place> GetAll(int? count = null, int? page = null);
        Task<Place[]> GetAllAsync(int? count = null, int? page = null);
        Task SaveAsync(Place place);
        Place[] FindByUser(string userId);
        Task<Place[]> FindByUserAsync(string userId);
        IList<PossibleSwaps> DetectPossibleSwaps (int maxDistance);


        // favorites
        Task<UserFavorites> FindUserFavoritesAsync(Guid userId);
        Task UpdateUserFavoritesAsync(UserFavorites favs);
        Task AddUserFavoritesAsync(UserFavorites favs);
    }
}
