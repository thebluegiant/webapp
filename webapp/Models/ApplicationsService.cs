using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Blitzmove.Data;

namespace Blitzmove.Models
{
    public class ApplicationsService : IApplicationsService
    {
        private ApplicationDbContext _context;

        private readonly IAdsService _adsService;

        public ApplicationsService( ApplicationDbContext context,  IAdsService adsService)
        {
            _context = context;
            _adsService = adsService;
        }

        public async Task<bool> UserHasAlreadyAppliedAsync(Guid userGuid, Guid placeGuid)
        {
            var listOfUserApps = await FindByUserAsync(userGuid.ToString());
            if(listOfUserApps==null) return false;

            return Array.Exists(listOfUserApps, item => item.PlaceAppliedToGuid == placeGuid);
        }


        public async Task UpdateAsync(ApplicationToAnAd applicationToAnAd)
        {
            _context.ApplicationToAds.Update(applicationToAnAd);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateRangeAsync(ApplicationToAnAd[] applicationToAnAd)
        {
            _context.ApplicationToAds.UpdateRange(applicationToAnAd);
            await _context.SaveChangesAsync();
        }

        public async Task AddAndSave(ApplicationToAnAd applicationToAnAd)
        {
            applicationToAnAd.CreationTime = DateTime.Now;
            _context.ApplicationToAds.Add(applicationToAnAd);
            await _context.SaveChangesAsync();
        }


        public Task<ApplicationToAnAd[]> FindByUserAsync(string userId)
        {
            return _context.ApplicationToAds.Where(x => x.Applicant.Id.Equals(userId)).ToArrayAsync();
        }


        public Task<ApplicationToAnAd[]> FindByPlaceAsync(Guid placeId)
        {
            return _context.ApplicationToAds.Where(x => x.PlaceAppliedToGuid.Equals(placeId)).Include(a => a.Applicant).ToArrayAsync();
        }


        public ApplicationToAnAd Find(Guid id)
        {
            return _context.ApplicationToAds
                .FirstOrDefault(x => x.Id.Equals(id));
        }


        public Task<ApplicationToAnAd> FindAsync(Guid id)
        {
            return _context.ApplicationToAds.FirstOrDefaultAsync(x => x.Id.Equals(id));
        }


        public async Task DeleteAsync(Guid id)
        {
            var app = await FindAsync(id);

            // update corresponding place
            var place = await _adsService.FindAsync(app.PlaceAppliedToGuid);
            if(place != null)
            {
                place.NumberOfApplicants -= 1;
                
                if(app.PromisedReward != 0) // Because 0 rewards aren't present in the table
                    place.Rewards.Remove(place.Rewards.Where(a => a.Value == app.PromisedReward).First());

                _context.Places.Update(place);
            }

            _context.ApplicationToAds.Remove(app);

            await _context.SaveChangesAsync();
        }

    }
}