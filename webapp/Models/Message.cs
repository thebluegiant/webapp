﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace Blitzmove.Models
{
    public class Message
    {
        public int Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid user1Archived  { get; set; }

        public Guid user2Archived  { get; set; }

        public Guid SenderGuid { get; set; }

        public Guid RecipientGuid { get; set; }

        public string Content { get; set; }

        public DateTime ViewedDateTime { get; set; }
    }
}
