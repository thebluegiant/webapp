using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using Newtonsoft.Json;
using Blitzmove.Models;


namespace Blitzmove.Services
{
    public class GoogleReCaptchaService
    {
        private ReCAPTCHASettings _settings;

        public GoogleReCaptchaService(IOptions<ReCAPTCHASettings> settings)
        {
            _settings = settings.Value;
        }

        public virtual async Task<GoogleREspo> VerifyToken (string _Token)
        {
            GoogleReCaptchaData _MyData = new GoogleReCaptchaData
            {
                response = _Token,
                secret=_settings.ReCAPTCHA_Secret_Key
            };

            HttpClient client = new HttpClient();

            var resp = await client.GetStringAsync($"https://www.google.com/recaptcha/api/siteverify?secret={_MyData.secret}&response={_MyData.response}");

            var capresp = JsonConvert.DeserializeObject<GoogleREspo>(resp);

            return capresp;
        }

    }

    public class GoogleReCaptchaData
    {
        public string response {get; set;} // token
        public string secret {get; set;}
    }

    public class GoogleREspo
    {
        public bool success {get; set;}
        public double score {get; set;}
        public string action {get; set;}
        public DateTime challenge_ts {get; set;}
        public string hostname {get; set;}
    }


}

