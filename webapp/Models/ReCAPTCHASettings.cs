using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blitzmove.Models
{
    public class ReCAPTCHASettings
    {
        public string ReCAPTCHA_Site_Key {get; set;}
        public string ReCAPTCHA_Secret_Key {get; set;}
    }
}