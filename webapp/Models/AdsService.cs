﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Blitzmove.Data;
using Blitzmove.Geo;
using Blitzmove.Algorithms;


namespace Blitzmove.Models
{
    public class PossibleSwaps
    {
        public Guid PlaceId {get; set;}

        public IList<Guid> CandidatesIds { get; set; } = new List<Guid>();
    } 

    public class AdsService : IAdsService
    {
        private ApplicationDbContext _context;
            
        public AdsService()
        {
            this.InitContext();
        }

        private async void InitContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>().Options;
            await _context.Database.EnsureCreatedAsync();
            _context = new ApplicationDbContext(options);
        }

        public AdsService(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<UserFavorites> FindUserFavoritesAsync(Guid userId)
        {
            return _context.Favorites.Include(a => a.PlaceIds)
                    .FirstOrDefaultAsync(x => x.UserId.Equals(userId));
        }

        public async Task UpdateUserFavoritesAsync(UserFavorites favs)
        {
            _context.Favorites.Update(favs);
            await _context.SaveChangesAsync();
        }

        public async Task AddUserFavoritesAsync(UserFavorites favs)
        {
            _context.Favorites.Add(favs);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            // _context.Places.Remove(this.Find(id));
            var place = this.Find(id);
            place.DeletionDateTime = DateTime.Now;
            _context.Places.Update(place);

            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Place place)
        {
            _context.Places.Update(place);
            await _context.SaveChangesAsync();
        }
        

        public Place Find(Guid id)
        {
            return _context.Places.Where(p => p.DeletionDateTime == DateTime.MinValue)
                                    .Include(a => a.Pictures).Include(a => a.Rewards)
                                    .FirstOrDefault(x => x.Id.Equals(id));
        }


        public Task<Place> FindAsync(Guid id)
        {
            return _context.Places.Where(p => p.DeletionDateTime ==DateTime.MinValue)
                                    .Include(a => a.Pictures).Include(a => a.Rewards)
                                    .FirstOrDefaultAsync(x => x.Id.Equals(id));
        }


        public Place[] FindByUser(string userId)
        {
            return _context.Places.Where(p => p.DeletionDateTime ==DateTime.MinValue)
                                    .Include(a => a.Pictures).Include(a => a.Rewards)
                                    .Where(x => x.UserId.Equals(userId)).ToArray();
        }

        public Task<Place[]> FindByUserAsync(string userId)
        {
            return _context.Places.Where(p => p.DeletionDateTime ==DateTime.MinValue)
                                    .Include(a => a.Pictures).Include(a => a.Rewards)
                                    .Where(x => x.UserId.Equals(userId)).ToArrayAsync();
        }

        public Task<Place[]> GetPlacesWithinAreaAsync(double ne_lng, double ne_lat, double sw_lng, double sw_lat)
        {
            return _context.Places.Where(p => p.DeletionDateTime == DateTime.MinValue && 
                                                ((p.GpsLongitude.Value - ne_lng) * (p.GpsLongitude.Value - sw_lng) < 0) && 
                                                ((p.GpsLatitude.Value - ne_lat) * (p.GpsLatitude.Value - sw_lat) < 0))
                                  .Include(a => a.Pictures)
                                  .Include(a => a.Rewards)
                                  .ToArrayAsync();
        }

        public IQueryable<Place> GetAll(int? count = null, int? page = null)
        {
            return _context.Places.Where(p => p.DeletionDateTime ==DateTime.MinValue)
                                    .Include(a => a.Pictures).Include(a => a.Rewards);
        }

        public Task<Place[]> GetAllAsync(int? count = null, int? page = null)
        {
            return GetAll(count, page).ToArrayAsync();
        }

        public async Task SaveAsync(Place place)
        {
            _context.Places.Add(place);
            await _context.SaveChangesAsync();
        }

        public IList<PossibleSwaps> DetectPossibleSwaps (int maxDistance = 1)
        {
            List<PossibleSwaps> list = new List<PossibleSwaps>();

            // select places containing wanted location
            var places = _context.Places.Where(p => p.NewAddress != null && p.DeletionDateTime == DateTime.MinValue);

            // running o(n^2) detection
            for(int i=0; i<places.Count(); i++)
            {
                PossibleSwaps element = new PossibleSwaps();
                element.PlaceId = places.ElementAt(i).Id;
                for(int j=0; j<places.Count(); j++)
                {
                    if(i != j)
                    {
                        if(Matching.PlacesMatches(places.ElementAt(i), places.ElementAt(j), maxDistance))
                        {
                            element.CandidatesIds.Add(places.ElementAt(j).Id);
                        }
                    }
                }
                if (element.CandidatesIds.Count() != 0) list.Add(element);
            }

            return list;
        }

    }
}
