﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Blitzmove.Models
{
    public interface IMessageService
    {
        Task ArchiveConversation (Guid userId, Guid contactId);
        Task SaveMessage(string senderId, string recipientId, string message);
        Task SaveMessageAndEmail(string senderId, string recipientId, string message, string emailContent);
        Task<List<Message>> GetAllUserMessages(string userId);
        Task<string> GetUsername(string applicationUserId);


        #region Methods that are not implemented yet
        void SendMessage(Message message, IdentityUser user);
        IList<Message> GetAllConversationsForUser(int userId);
        int GetNewMessagesCount(int userId);
        void SetMessageViewed(int messageId);
        #endregion
    }
}
