﻿using System;

namespace Blitzmove.Models
{
    public class ApplicationToAnAd
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public DateTime CreationTime { get; set; }

        public ApplicationUser Applicant { get; set; }

        public Guid PlaceAppliedToGuid { get; set; }

        public int PromisedReward { get; set; }

        public string Details { get; set; }


        // -2 disabled
        // -1 under review
        //  0 declined
        //  1 invited
        //  2 payment request sent
        //  3 payment to be released by either the poster or the applicant 
        public int Status { get; set; } = -1; 
    }
}
