﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Blitzmove.Data;
using Blitzmove.Services;


namespace Blitzmove.Models
{
    public class MessageService : IMessageService
    {
        private ApplicationDbContext _context;
        private readonly ICustomEmailSender _emailSender;
        private readonly UserManager<ApplicationUser> _userManager;


        public MessageService(ApplicationDbContext context, ICustomEmailSender emailSender, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _emailSender = emailSender;
            _userManager = userManager;
        }

        public async Task SaveMessage(string senderId, string recipientId, string message)
        {
            _context.Messages.Add(new Message
            {
                SenderGuid = Guid.Parse(senderId),
                RecipientGuid = Guid.Parse(recipientId),
                CreatedDateTime = DateTime.Now,
                Content = message
            });

            await _context.SaveChangesAsync();            
        }

        public async Task SaveMessageAndEmail(string senderId, string recipientId, string message, string emailContent)
        {
            await SaveMessage(senderId, recipientId, message);

            // sending email
            var sender = await _userManager.FindByIdAsync(senderId);
            var recipient = await _userManager.FindByIdAsync(recipientId);
            var senderName = sender.Name;
            var title = recipient.Name + ", you have a new message";

            _emailSender.ConfigureSender(senderName);
            await _emailSender.SendEmailAsync(recipient.Email, title, emailContent);
        }


        public async Task ArchiveConversation(Guid userGuid, Guid contactGuid)
        {
            var messagesToArchive = _context.Messages.Where(m => 
                                    (m.user1Archived != userGuid && m.user2Archived != userGuid) && 
                                    ( (contactGuid.Equals(m.RecipientGuid) && userGuid.Equals(m.SenderGuid)) || 
                                      (contactGuid.Equals(m.SenderGuid) && userGuid.Equals(m.RecipientGuid))) );

            foreach(var message in messagesToArchive)
            {
                bool isArchived = false;

                if (message.user1Archived == Guid.Empty)
                {
                    message.user1Archived = userGuid;
                    isArchived = true;
                }

                if (message.user2Archived == Guid.Empty && !isArchived)
                {
                    message.user2Archived = userGuid;
                }
            }
            
            _context.Messages.UpdateRange(messagesToArchive);

            await _context.SaveChangesAsync();
        }

        public Task<List<Message>> GetAllUserMessages(string userId)
        {
            var userGuid = Guid.Parse(userId);
            return _context.Messages.Where(m => (m.user1Archived != userGuid && m.user2Archived != userGuid) && 
                                                (userGuid.Equals(m.RecipientGuid) || userGuid.Equals(m.SenderGuid)))
                                    .ToListAsync();
        }

        public async Task<string> GetUsername(string applicationUserId)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => string.Equals(u.Id, applicationUserId));
            return user == null ? string.Empty: user.Name;
        }




        public void SendMessage(Message message, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public IList<Message> GetAllConversationsForUser(int userId)
        {
            throw new NotImplementedException();
        }

        public int GetNewMessagesCount(int userId)
        {
            throw new NotImplementedException();
        }

        public void SetMessageViewed(int messageId)
        {
            throw new NotImplementedException();
        }
    }
}