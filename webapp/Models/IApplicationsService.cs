using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blitzmove.Models
{
    public interface IApplicationsService
    {
        Task<bool> UserHasAlreadyAppliedAsync(Guid userGuid, Guid placeGuid);
        Task UpdateAsync(ApplicationToAnAd applicationToAnAd);
        Task UpdateRangeAsync(ApplicationToAnAd[] applicationToAnAd);
        Task AddAndSave(ApplicationToAnAd applicationToAnAd);
        Task<ApplicationToAnAd[]> FindByUserAsync(string userId);
        Task<ApplicationToAnAd[]> FindByPlaceAsync(Guid placeId);
        ApplicationToAnAd Find(Guid application);
        Task<ApplicationToAnAd> FindAsync(Guid id);
        Task DeleteAsync(Guid id); // TODO: change deletionTime like places
    }
}