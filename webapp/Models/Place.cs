﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static System.Environment;
using System;
using System.Text;
using System.Linq;

namespace Blitzmove.Models
{
    public class Picture
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public int PicOrder { get; set; }

        public string ContentType {get; set;}

        public byte[] Data {get; set;}
    }

    public class Reward
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid ApplicantId { get; set;}
        public int Value { get; set; }
    }

    public class Place
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; } = Guid.NewGuid();

        public DateTime DeletionDateTime { get; set; }

        public int VisitorCount {get; set;} = 0;

        public string UserId { get; set; } 
        

        // validation
        // max items: 20
        // max size per pic: 7 mb
        // formats: jpeg/jpg, png
        [Required]
        public IList<Picture> Pictures { get; set; } = new List<Picture>();
        
        // wanted address (optional fields)
        public string NewAddress { get; set;}
        public double? NewGpsLatitude  { get; set; }
        public double? NewGpsLongitude  { get; set; }

        // current address
        [Required]
        public string Address { get; set;}

        [Required]
        public double? GpsLatitude  { get; set; }

        [Required]
        public double? GpsLongitude  { get; set; }
        
        [Required]
        public string Description { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int? Rent { get; set; }

        [Required]
        public int Bedrooms { get; set; }

        public string GetInlinePictureSrc(int index)
        {
            if (index < 0) return null;

            if (Pictures.ElementAt(index).Data == null || Pictures.ElementAt(index).ContentType == null)
                return null;

            var base64Image = System.Convert.ToBase64String(Pictures.ElementAt(index).Data);
            return $"data:{Pictures.ElementAt(index).ContentType};base64,{base64Image}";
        }
    

        /* metadata Section*/
        [Required]
        public DateTime PostDate { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int NumberOfApplicants { get; set; }
        
        public IList<Reward> Rewards { get; set; } = new List<Reward>();
        /* End of metadata section */
    }
}
