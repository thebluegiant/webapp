using Microsoft.AspNetCore.Identity.UI.Services;
using System.Threading.Tasks;



namespace Blitzmove.Services
{
    public interface ICustomEmailSender : IEmailSender
    {
        void ConfigureSender(string senderName, string senderEmail = "noreply@pickamove.com");
    }
}
