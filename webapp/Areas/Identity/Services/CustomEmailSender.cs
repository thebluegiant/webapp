using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace Blitzmove.Services
{
    public class CustomEmailSender : ICustomEmailSender
    {
        public string SenderName {get; set;} = null;
        public string SenderEmail {get; set;}

        public CustomEmailSender()
        {
        }

        public void ConfigureSender(string senderName, string senderEmail)
        {
            SenderName  = senderName;
            SenderEmail = senderEmail;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            // var apiKey = System.Environment.GetEnvironmentVariable("SENDGRID_API_KEY");
            return Execute("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", subject, message, email);
        }

        public Task Execute(string apiKey, string subject, string message, string email)
        {
            var client = new SendGridClient(apiKey);

            var msg = new SendGridMessage()
            {
                From = new EmailAddress(SenderEmail, SenderName),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            
            msg.AddTo(new EmailAddress(email));

            // Disable click tracking.
            // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
            msg.SetClickTracking(false, false);

            return client.SendEmailAsync(msg);
        }
    }
}