﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Blitzmove.Services;

namespace Blitzmove.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly ICustomEmailSender _emailSender;
        private readonly IMessageService _messagesService;

        public Guid SuperContactId {get; private set;} =  Guid.Parse("00000000-0000-0000-0000-000000000777");

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            ICustomEmailSender emailSender,
            IMessageService msgService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _messagesService = msgService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public const string MessageKey = nameof(MessageKey);

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 7)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { Name = Input.Email.Substring(0,Input.Email.IndexOf('@')), 
                                                 UserName = Input.Email,
                                                 Email = Input.Email };

                var result = await _userManager.CreateAsync(user, Input.Password);
                if (false)
                {
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page("/Account/ConfirmEmail",
                                                pageHandler: null,
                                                values: new { userId = user.Id, code = code },
                                                protocol: Request.Scheme);

                    string htmlmsg = "Please confirm your account by clicking on the button below: <br><br><br><br>" +
                                    $"<div style='text-align:center;'> <a style='text-decoration: none;padding-top:10px;padding-bottom:10px;padding-right:20px;padding-left:20px;border-radius:50px!important;background:#ff8138;color:white;' href='{HtmlEncoder.Default.Encode(callbackUrl)}'><b>CONFIRM</b></a></div> <br><br><br>" +
                                    $"Or you can paste the following link into your browser: <br>  <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>{HtmlEncoder.Default.Encode(callbackUrl)}</a> <br>" +
                                    "Once you do that, you should automatically be signed in. <br><br>" +
                                    "Thanks for using Pickamove ! <br><br>" +
                                    "The Pickamove Team.";

                    _emailSender.ConfigureSender("Pickamove");
                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email address", htmlmsg);

                    // await _signInManager.SignInAsync(user, isPersistent: false);
                    // return LocalRedirect(returnUrl);
                    TempData[MessageKey] = "Please check your inbox to confirm your email address. Thank you !";
                    return RedirectToAction(Request.Path);
                }

                if(result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    // send bot message
                    string msg = "Welcome to Pickamove " + user.Name + " ! I am a bot and my job is to notify you to make things work great for you.";
                    await _messagesService.SaveMessage(SuperContactId.ToString(), user.Id, msg);

                    return LocalRedirect(returnUrl);
                }
                
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
