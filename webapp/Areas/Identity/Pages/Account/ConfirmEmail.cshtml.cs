using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Blitzmove.Models;
using Blitzmove.Services;
using System.Text.Encodings.Web;


namespace Blitzmove.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ICustomEmailSender _emailSender;
        private readonly IMessageService _messagesService;

        public Guid SuperContactId {get; private set;} =  Guid.Parse("00000000-0000-0000-0000-000000000777");

        public ConfirmEmailModel(UserManager<ApplicationUser> userManager, 
                                 SignInManager<ApplicationUser> signInManager,
                                 ICustomEmailSender emailSender,
                                 IMessageService msgService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _messagesService = msgService;
        }

        public async Task<IActionResult> OnGetAsync(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToPage("/Index");
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{userId}'.");
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);

            if (!result.Succeeded)
            {
                throw new InvalidOperationException($"Error confirming email for user with ID '{userId}':");
            }
            
            // in case we succeed, then the user is logged in
            await _signInManager.SignInAsync(user, isPersistent: false);

            // Send welcome email
            var callbackUrl = Url.Page("/CreatePlace", pageHandler: null, values: null, protocol: Request.Scheme);


            string htmlmsg = $"<h2 style='font-weight:100;'> {HtmlEncoder.Default.Encode(char.ToUpper(user.Name[0]) + user.Name.Substring(1))}, Welcome to Pickamove ! </h2> <br><br><br><br>" +
                             "You can start using it by adding your current place: <br><br><br><br>" + 
                             $"<div style='text-align:center;'> <a style='text-decoration: none;padding-top:10px;padding-bottom:10px;padding-right:20px;padding-left:20px;border-radius:50px!important;background:#ff8138;color:white;' href='{HtmlEncoder.Default.Encode(callbackUrl)}'><b>ADD MY PLACE</b></a></div> <br><br><br>" +
                             "Once added, you will be notified as soon as someone is interested. <br><br>" +
                             "Thanks for using Pickamove ! <br><br>" +
                             "The Pickamove Team.";

            _emailSender.ConfigureSender("Pickamove");
            await _emailSender.SendEmailAsync(user.Email, "Welcome to Pickamove", htmlmsg);

            // send bot message
            string msg = "Welcome to Pickamove " + user.Name + " ! I am a bot and my job is to notify you to make things work great for you.";
            await _messagesService.SaveMessage(SuperContactId.ToString(), user.Id, msg);

            return LocalRedirect(Url.Content("~/"));
        }
    }
}
