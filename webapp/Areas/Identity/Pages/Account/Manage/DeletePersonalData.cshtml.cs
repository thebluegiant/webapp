using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Blitzmove.Models;
using Blitzmove.Services;

namespace Blitzmove.Areas.Identity.Pages.Account.Manage
{
    public class DeletePersonalDataModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<DeletePersonalDataModel> _logger;
        private readonly IAdsService  _adsService;
        private readonly IApplicationsService _appsService;
        private readonly ICustomEmailSender _emailSender;


        public DeletePersonalDataModel( UserManager<ApplicationUser> userManager,
                                        SignInManager<ApplicationUser> signInManager,
                                        ILogger<DeletePersonalDataModel> logger, 
                                        IAdsService adsService,
                                        IApplicationsService appsService,
                                        ICustomEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _adsService = adsService;
            _appsService = appsService;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }

        public bool RequirePassword { get; set; }

        public async Task<IActionResult> OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            RequirePassword = await _userManager.HasPasswordAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            RequirePassword = await _userManager.HasPasswordAsync(user);
            if (RequirePassword)
            {
                if (!await _userManager.CheckPasswordAsync(user, Input.Password))
                {
                    ModelState.AddModelError(string.Empty, "Password not correct.");
                    return Page();
                }
            }
            
            // deleting applications made by user
            var applications = await _appsService.FindByUserAsync(user.Id);
            foreach(var a in applications)
            {
                await _appsService.DeleteAsync(a.Id);
            }

            // deleting places posted by user
            var places = await _adsService.FindByUserAsync(user.Id);
            foreach(var p in places)
            {
                await _adsService.DeleteAsync(p.Id);
            }

            // deleting user
            var result = await _userManager.DeleteAsync(user);

            var userId = await _userManager.GetUserIdAsync(user);
            if (!result.Succeeded)
            {
                throw new InvalidOperationException($"Unexpected error occurred while deleting user with ID '{userId}'.");
            }

            // sending goodbye email
            string title = user.Name + ", we are sorry to see you go";
            string htmlmsg = "Your account has been deleted.";  
            _emailSender.ConfigureSender("Pickamove");
            await _emailSender.SendEmailAsync(user.Email, title, htmlmsg);   

            // signing out
            await _signInManager.SignOutAsync();

            _logger.LogInformation("User with ID '{UserId}' deleted themselves.", userId);

            return Redirect("~/");
        }
    }
}