﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;
using Blitzmove.Data;


namespace Blitzmove.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;

        public IndexModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        public string CurrentPicture {get; set;}

        [BindProperty]
        [DataType(DataType.Upload)]
        [MaxFileSize(11*1024*1024)]
        [AllowedExtensions(new string[] {".jpg", ".png", ".jpeg"})]
        public IFormFile PictureFile { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [StringLength(15, ErrorMessage = "The {0} must be at maximum {1} characters long.")]
            public string Username { get; set; }

            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }

            [Display(Name = "Approximate yearly salary")]
            public int Salary { get; set; }

            [Display(Name = "Job title")]
            public string JobTitle { get; set; }

            [Display(Name = "Current credit score")]
            public int CreditScore { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var email = await _userManager.GetEmailAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);

            var jobTitle = user.JobTitle;
            var salary = user.Salary;
            var creditScore = user.CreditScore;
            var username = user.Name;

            Input = new InputModel
            {
                Username = username,
                Email = email,
                PhoneNumber = phoneNumber,

                JobTitle = jobTitle,
                Salary = salary,
                CreditScore = creditScore,
            };

            CurrentPicture = user.ProfilePicture;

            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);

            return Page();
        }

        public string GetInlinePictureSrc(Picture pic)
        {
            if (pic == null) return null;

            var base64Image = System.Convert.ToBase64String(pic.Data);
            return $"data:{pic.ContentType};base64,{base64Image}";
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var email = await _userManager.GetEmailAsync(user);
            if (Input.Email != email)
            {
                var setEmailResult = await _userManager.SetEmailAsync(user, Input.Email);
                if (!setEmailResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                }
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting phone number for user with ID '{userId}'.");
                }
            }

            var jobTitle = user.JobTitle;
            var salary = user.Salary;
            var creditScore = user.CreditScore;
            var username = user.Name;

            if (Input.JobTitle != jobTitle)
            {
                user.JobTitle = Input.JobTitle;
            }
            if (Input.Salary != salary)
            {
                user.Salary = Input.Salary;
            }
            if (Input.CreditScore != creditScore)
            {
                user.CreditScore = Input.CreditScore;
            }
            if (Input.Username != username)
            {
                user.Name = Input.Username;
            }

            if (this.PictureFile != null)
            {
                using(var stream = new System.IO.MemoryStream())
                {
                    await PictureFile.CopyToAsync(stream);
                    user.ProfilePicture = GetInlinePictureSrc(new Picture
                    {
                        Data = stream.ToArray(),
                        ContentType = PictureFile.ContentType,
                        PicOrder = 0
                    });
                }
            }
            
            await _userManager.UpdateAsync(user);

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Your profile has been updated";
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostDeleteProfilePictureAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            user.ProfilePicture = null;
            await _userManager.UpdateAsync(user);

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }


            var userId = await _userManager.GetUserIdAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { userId = userId, code = code },
                protocol: Request.Scheme);
            await _emailSender.SendEmailAsync(
                email,
                "Confirm your email",
                $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

            StatusMessage = "Verification email sent. Please check your email.";
            return RedirectToPage();
        }
    }
}
