﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Blitzmove.Models;
using Microsoft.AspNetCore.Authorization;

namespace Blitzmove.Hubs
{
    [Authorize] 
    public class MessageHub : Hub
    {
        private readonly IMessageService _messagesService;

        public MessageHub(IMessageService messagesService)
        {
            _messagesService = messagesService;
        }

        // todo need some security checks at all db write methods. 
        public async Task SendMsg(string senderId, string recipientId, string message)
        {
            if(string.IsNullOrEmpty(message) || string.IsNullOrEmpty(recipientId) 
                                             || string.IsNullOrEmpty(senderId)
                                             || string.Equals(Guid.Empty.ToString(), recipientId)
                                             || string.Equals(Guid.Empty.ToString(), senderId))
            {
                return;
            }

            await _messagesService.SaveMessage(senderId, recipientId, message);
//            var userId = Context.User.FindFirstValue(ClaimTypes.NameIdentifier);
//            var id = Context.UserIdentifier;  // should not be null, used by SignalR
            var recipient = Clients.User(recipientId);
            await recipient.SendAsync("ReceiveMessage", senderId, message);
        }
    }
}