using System;
using System.IO;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;


namespace Blitzmove.Data
{

public class MaxFileSizeAttribute : ValidationAttribute
{
    private readonly int _maxFileSize;
    public MaxFileSizeAttribute(int maxFileSize)
    {
        _maxFileSize = maxFileSize;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        bool isList = value?.GetType() == typeof(List<IFormFile>);

        if(isList)
        {
            var files = value as List<IFormFile>;
            foreach (var file in files)
            {
                if (file != null)
                {
                    if (file.Length > _maxFileSize)
                    {
                        return new ValidationResult(GetErrorMessage());
                    }
                }
            }
            return ValidationResult.Success;
        }
        else
        {
            var file = value as IFormFile;
            if (file != null)
            {
                if (file.Length > _maxFileSize)
                {
                    return new ValidationResult(GetErrorMessage());
                }
            }
            return ValidationResult.Success;
        }
    }

    public string GetErrorMessage()
    {
        return $"Maximum allowed file size is { _maxFileSize / (1024*1024)} MB.";
    }
}


public class AllowedExtensionsAttribute : ValidationAttribute
{
    private readonly string[] _Extensions;
    public AllowedExtensionsAttribute(string[] Extensions)
    {
        _Extensions = Extensions;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        bool isList = value?.GetType() == typeof(List<IFormFile>);

        if(isList)
        {
            var files = value as List<IFormFile>;
            foreach (var file in files)
            {
                var extension = Path.GetExtension(file.FileName);
                if (!(file == null))
                {
                    if (!_Extensions.Contains(extension.ToLower()))
                    {
                        return new ValidationResult(GetErrorMessage());
                    }
                }
            }
            return ValidationResult.Success;
        }
        else
        {
            var file = value as IFormFile;
            var extension = Path.GetExtension(file.FileName);
            if (!(file == null))
            {
                if (!_Extensions.Contains(extension.ToLower()))
                {
                    return new ValidationResult(GetErrorMessage());
                }
            }
            return ValidationResult.Success;
        }
    }

    public string GetErrorMessage()
    {
        return $"Please select pictures with jpeg/jpg/png extension.";
    }
}

}