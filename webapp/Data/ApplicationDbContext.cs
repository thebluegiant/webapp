﻿using System;
using System.Collections.Generic;
using System.Text;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;


namespace Blitzmove.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            //if(this.Database.GetService<IRelationalDatabaseCreator>().Exists())  this.EnsureSeedData();
        }
        
        public DbSet<Place> Places { get; set; }

        public DbSet<UserFavorites> Favorites { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<ApplicationToAnAd> ApplicationToAds { get; set; }
    }
}
