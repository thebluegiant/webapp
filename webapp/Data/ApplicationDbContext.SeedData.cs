﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blitzmove.Models;
using Blitzmove.Geo;


namespace Blitzmove.Data
{
    public static class ApplicationDbContextSeedData
    {
        static object synchlock = new object();
        static volatile bool seeded = false;
        public static void EnsureSeedData(this ApplicationDbContext context)
        {
            if (!seeded && context.Places.Count() == 0)
            {
                lock (synchlock)
                {
                    if (!seeded)
                    {
                        var Ads = GenerateAds(100, 100);
                        context.Places.AddRange(Ads);
                        context.SaveChanges();
                        seeded = true;
                    }
                }
            }
        }

        public static Place[] GenerateAds(int city1, int city2)
        {
            List<Place> allLocations = new List<Place>();
            CoordinateGenerator gen = new CoordinateGenerator();

            Random genrand = new Random();

            // city1 locations (gps from https://boundingbox.klokantech.com/ GEOJSON)
            Coordinates[] city1Locations = gen.Calculate(city1, new Coordinates(-75.6985869199,45.2136474445),
                                                                new Coordinates(-75.8522877561,45.3795803806), 
                                                                new Coordinates(-75.582725396,45.5028176534), 
                                                                new Coordinates(-75.4290245599,45.3372462503));
            for (int i=0; i<city1; i++)
            {
                int prent = genrand.Next(200, 7000);
                int pbedrooms = genrand.Next(0,7);
                // int pminbonus = genrand.Next(300);
                // string pdate  = genrand.Next(1,12).ToString() +"/" + genrand.Next(1,12).ToString() + "/2019";

                allLocations.Add(new Place
                {
                    NewAddress = "Gatineau Park",
                    NewGpsLatitude = 45.5060,
                    NewGpsLongitude = -75.8120,
                    Rent = prent,
                    Bedrooms = pbedrooms,
                    Address = "Ottawa, Canada",
                    GpsLatitude = city1Locations[i].Latitude, 
                    GpsLongitude = city1Locations[i].Longitude,
                    Description = "Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description.",
                    PostDate = Convert.ToDateTime("04/11/2019"),
                    NumberOfApplicants = 0,
                    // MinBonus = pminbonus,
                    // Title = "Condo in Ottawa",
                    // AvailabilityDate = Convert.ToDateTime(pdate)
                });
            }

            // city2 locations
            Coordinates[] city2Locations = gen.Calculate(city2, new Coordinates(-79.7149042985,43.4932116989),
                                                                new Coordinates(-79.9985358151,43.860847471), 
                                                                new Coordinates(-79.3342598574,44.1274987374), 
                                                                new Coordinates(-79.0506283408,43.7615057161));
            for (int i=0; i<city2; i++)
            {
                int prent     = genrand.Next(200,7000);
                int pbedrooms = genrand.Next(0,7);
                // int pminbonus = genrand.Next(300);
                // string pdate  = genrand.Next(1,12).ToString() +"/" + genrand.Next(1,12).ToString() + "/2019";

                allLocations.Add(new Place
                {
                    NewAddress = "Ajax",
                    NewGpsLatitude = 43.85012,
                    NewGpsLongitude = -79.03288,
                    Rent = prent,
                    Bedrooms = pbedrooms,
                    Address = "Toronto",
                    GpsLatitude = city2Locations[i].Latitude, 
                    GpsLongitude = city2Locations[i].Longitude,
                    Description = "Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description.",
                    PostDate = Convert.ToDateTime("04/11/2019"),
                    NumberOfApplicants = 0,
                    // MinBonus = pminbonus,
                    // Title = "Condo in Toronto",
                    // AvailabilityDate = Convert.ToDateTime(pdate)
                });
            }

            return allLocations.ToArray();
        }
    }
    
}

