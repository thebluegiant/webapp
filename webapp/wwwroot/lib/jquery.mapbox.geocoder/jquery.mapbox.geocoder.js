! function(e) {
    var t = "mapboxAutocomplete",
        a = {
            accessToken: "",
            endpoint: "https://api.mapbox.com/geocoding/v5/",
            mode: "mapbox.places",
            types: "place",
            countries: "us,ca",
            language: "en",
            width: "100%",
            zindex: "1000"
        };

    function n(n, s) {
        return this.element = e(n), this.$elem = e(this.element), this._name = t, this.settings = e.extend({}, a, s), this._defaults = a, this.init()
    }
    n.prototype = {
        options: function(e, t) {
            this.settings[e] = t
        },
        init: function() {
            var t = this.element;
            t.wrap('<div class="address-autocomplete-wrapper"></div>');
            var a = e(".address-autocomplete-wrapper");
            t.after('<input hidden name="mbaa-found-address" value="" />'), t.after('<ul id="mbaa-result-address-autocomplete" class="mbaa-results-list"></ul>');
            var n, s = e("#mbaa-result-address-autocomplete"),
                i = "",
                d = null,
                o = function(t) {
                    var a = t.place_name.split(", ")[0],
                        n = t.address;
                    a = a.replace(n + " ", "");
                    var s = {
                        point: {
                            lat: t.center[1],
                            long: t.center[0]
                        },
                        formatted_address: t.place_name,
                        region: "",
                        country: "",
                        city: "",
                        zipcode: "",
                        street: a,
                        number: n
                    };
                    return e.each(t.context, function(e, t) {
                        0 <= t.id.indexOf("place") ? s.city = t.text : 0 <= t.id.indexOf("postcode") ? s.zipcode = t.text : 0 <= t.id.indexOf("region") ? s.region = t.text : 0 <= t.id.indexOf("country") && (s.country_long = t.text, s.country = t.short_code)
                    }), s
                };
            t.unbind("keydown").on("keydown", function(e) {
                if (40 === e.keyCode) return "" === i ? i = 0 : i + 1 < s.find("li").length && i++, s.find("li").removeClass("selected"), s.find("li:eq(" + i + ")").addClass("selected"), !1;
                38 === e.keyCode && ("" === i ? i = 0 : 0 < i && i--, s.find("li").removeClass("selected"), s.find("li:eq(" + i + ")").addClass("selected"))
            }), t.on("keydown", function(a) {
                if (13 === a.keyCode) {
                    a.preventDefault();
                    var n = s.find("li.mbaa.selected").data("id");
                    e.each(d, function(a, i) {
                        if (i.id === n) {
                            var d = o(i);
                            e('input[name="mbaa-found-address"]').val(JSON.stringify(d)), s.removeClass("mbaa-fill"), t.val(d.formatted_address).removeClass("mbaa-address-autocomplete").trigger("mapboxAutocomplete.found.address", [d, i])
                        }
                    })
                }
            }), n = this.settings, a.css("width", n.width), t.css("width", n.width), s.css("width", n.width), a.find("ul.mbaa-results-list").css("z-index", n.zindex), t.on("keyup", function(a) {
                if (40 !== a.keyCode && 38 !== a.keyCode && 13 !== a.keyCode) {
                    i = "";
                    var r = (l = e(this).val(), c = n, l = encodeURI(l), c.endpoint + c.mode + "/" + l + ".json?types=" + c.types + "&country=" + c.countries + "&access_token=" + c.accessToken + "&language=" + c.language);
                    e.get(r, function(a) {
                        t.addClass("mbaa-address-autocomplete"), s.find("li.mbaa").remove(), s.addClass("mbaa-fill"), a.features && (d = a.features, e.each(d, function(e, t) {
                            s.append('<li data-id="' + t.id + '" class="mbaa">' + t.place_name + "</li>")
                        }), s.find("li").each(function(a, n) {
                            e(this).on("click", function() {
                                var a = e(this).data("id");
                                e.each(d, function(n, i) {
                                    if (i.id === a) {
                                        var d = o(i);
                                        e('input[name="mbaa-found-address"]').val(JSON.stringify(d)), s.removeClass("mbaa-fill"), t.val(d.formatted_address).removeClass("mbaa-address-autocomplete").trigger("mapboxAutocomplete.found.address", [d, i])
                                    }
                                })
                            })
                        }))
                    })
                }
                var l, c
            })
        }
    }, e.fn[t] = function(a) {
        var s = e.makeArray(arguments).slice(1);
        return this.each(function() {
            var i = e.data(this, t);
            if (!i) {
                var d = new n(this, a);
                return e.data(this, t, d), d
            }
            i[a] ? i[a].apply(i, s) : e.error("Method " + a + " does not exist on Plugin ?")
        })
    }
}(jQuery),
function(e) {
    var t = "mapboxAutocomplete2",
        a = {
            accessToken: "",
            endpoint: "https://api.mapbox.com/geocoding/v5/",
            mode: "mapbox.places",
            types: "address",
            countries: "us,ca",
            language: "en",
            width: "100%",
            zindex: "1000"
        };

    function n(n, s) {
        return this.element = e(n), this.$elem = e(this.element), this._name = t, this.settings = e.extend({}, a, s), this._defaults = a, this.init()
    }
    n.prototype = {
        options: function(e, t) {
            this.settings[e] = t
        },
        init: function() {
            var t = this.element;
            t.wrap('<div class="address-autocomplete-wrapper"></div>');
            var a = e(".address-autocomplete-wrapper");
            t.after('<input hidden name="mbaa-found-address" value="" />'), t.after('<ul id="mbaa-result-address-autocomplete2" class="mbaa-results-list"></ul>');
            var n, s = e("#mbaa-result-address-autocomplete2"),
                i = "",
                d = null,
                o = function(t) {
                    var a = t.place_name.split(", ")[0],
                        n = t.address;
                    a = a.replace(n + " ", "");
                    var s = {
                        point: {
                            lat: t.center[1],
                            long: t.center[0]
                        },
                        formatted_address: t.place_name,
                        region: "",
                        country: "",
                        city: "",
                        zipcode: "",
                        street: a,
                        number: n
                    };
                    return e.each(t.context, function(e, t) {
                        0 <= t.id.indexOf("place") ? s.city = t.text : 0 <= t.id.indexOf("postcode") ? s.zipcode = t.text : 0 <= t.id.indexOf("region") ? s.region = t.text : 0 <= t.id.indexOf("country") && (s.country_long = t.text, s.country = t.short_code)
                    }), s
                };
            t.unbind("keydown").on("keydown", function(e) {
                if (40 === e.keyCode) return "" === i ? i = 0 : i + 1 < s.find("li").length && i++, s.find("li").removeClass("selected"), s.find("li:eq(" + i + ")").addClass("selected"), !1;
                38 === e.keyCode && ("" === i ? i = 0 : 0 < i && i--, s.find("li").removeClass("selected"), s.find("li:eq(" + i + ")").addClass("selected"))
            }), t.on("keydown", function(a) {
                if (13 === a.keyCode) {
                    a.preventDefault();
                    var n = s.find("li.mbaa.selected").data("id");
                    e.each(d, function(a, i) {
                        if (i.id === n) {
                            var d = o(i);
                            e('input[name="mbaa-found-address"]').val(JSON.stringify(d)), s.removeClass("mbaa-fill"), t.val(d.formatted_address).removeClass("mbaa-address-autocomplete").trigger("mapboxAutocomplete.found.address", [d, i])
                        }
                    })
                }
            }), n = this.settings, a.css("width", n.width), t.css("width", n.width), s.css("width", n.width), a.find("ul.mbaa-results-list").css("z-index", n.zindex), t.on("keyup", function(a) {
                if (40 !== a.keyCode && 38 !== a.keyCode && 13 !== a.keyCode) {
                    i = "";
                    var r = (l = e(this).val(), c = n, l = encodeURI(l), c.endpoint + c.mode + "/" + l + ".json?types=" + c.types + "&country=" + c.countries + "&access_token=" + c.accessToken + "&language=" + c.language);
                    e.get(r, function(a) {
                        t.addClass("mbaa-address-autocomplete"), s.find("li.mbaa").remove(), s.addClass("mbaa-fill"), a.features && (d = a.features, e.each(d, function(e, t) {
                            s.append('<li data-id="' + t.id + '" class="mbaa">' + t.place_name + "</li>")
                        }), s.find("li").each(function(a, n) {
                            e(this).on("click", function() {
                                var a = e(this).data("id");
                                e.each(d, function(n, i) {
                                    if (i.id === a) {
                                        var d = o(i);
                                        e('input[name="mbaa-found-address"]').val(JSON.stringify(d)), s.removeClass("mbaa-fill"), t.val(d.formatted_address).removeClass("mbaa-address-autocomplete").trigger("mapboxAutocomplete.found.address", [d, i])
                                    }
                                })
                            })
                        }))
                    })
                }
                var l, c
            })
        }
    }, e.fn[t] = function(a) {
        var s = e.makeArray(arguments).slice(1);
        return this.each(function() {
            var i = e.data(this, t);
            if (!i) {
                var d = new n(this, a);
                return e.data(this, t, d), d
            }
            i[a] ? i[a].apply(i, s) : e.error("Method " + a + " does not exist on Plugin ?")
        })
    }
}(jQuery),
function(e) {
    var t = "mapboxAutocomplete3",
        a = {
            accessToken: "",
            endpoint: "https://api.mapbox.com/geocoding/v5/",
            mode: "mapbox.places",
            types: "poi",
            countries: "us,ca",
            language: "en",
            width: "100%",
            zindex: "1000"
        };

    function n(n, s) {
        return this.element = e(n), this.$elem = e(this.element), this._name = t, this.settings = e.extend({}, a, s), this._defaults = a, this.init()
    }
    n.prototype = {
        options: function(e, t) {
            this.settings[e] = t
        },
        init: function() {
            var t = this.element;
            t.wrap('<div class="address-autocomplete-wrapper"></div>');
            var a = e(".address-autocomplete-wrapper");
            t.after('<input hidden name="mbaa-found-address" value="" />'), t.after('<ul id="mbaa-result-address-autocomplete3" class="mbaa-results-list"></ul>');
            var n, s = e("#mbaa-result-address-autocomplete3"),
                i = "",
                d = null,
                o = function(t) {
                    var a = t.place_name.split(", ")[0],
                        n = t.address;
                    a = a.replace(n + " ", "");
                    var s = {
                        point: {
                            lat: t.center[1],
                            long: t.center[0]
                        },
                        formatted_address: t.place_name,
                        region: "",
                        country: "",
                        city: "",
                        zipcode: "",
                        street: a,
                        number: n
                    };
                    return e.each(t.context, function(e, t) {
                        0 <= t.id.indexOf("place") ? s.city = t.text : 0 <= t.id.indexOf("postcode") ? s.zipcode = t.text : 0 <= t.id.indexOf("region") ? s.region = t.text : 0 <= t.id.indexOf("country") && (s.country_long = t.text, s.country = t.short_code)
                    }), s
                };
            t.unbind("keydown").on("keydown", function(e) {
                if (40 === e.keyCode) return "" === i ? i = 0 : i + 1 < s.find("li").length && i++, s.find("li").removeClass("selected"), s.find("li:eq(" + i + ")").addClass("selected"), !1;
                38 === e.keyCode && ("" === i ? i = 0 : 0 < i && i--, s.find("li").removeClass("selected"), s.find("li:eq(" + i + ")").addClass("selected"))
            }), t.on("keydown", function(a) {
                if (13 === a.keyCode) {
                    a.preventDefault();
                    var n = s.find("li.mbaa.selected").data("id");
                    e.each(d, function(a, i) {
                        if (i.id === n) {
                            var d = o(i);
                            e('input[name="mbaa-found-address"]').val(JSON.stringify(d)), s.removeClass("mbaa-fill"), t.val(d.formatted_address).removeClass("mbaa-address-autocomplete").trigger("mapboxAutocomplete.found.address", [d, i])
                        }
                    })
                }
            }), n = this.settings, a.css("width", n.width), t.css("width", n.width), s.css("width", n.width), a.find("ul.mbaa-results-list").css("z-index", n.zindex), t.on("keyup", function(a) {
                if (40 !== a.keyCode && 38 !== a.keyCode && 13 !== a.keyCode) {
                    i = "";
                    var r = (l = e(this).val(), c = n, l = encodeURI(l), c.endpoint + c.mode + "/" + l + ".json?types=" + c.types + "&country=" + c.countries + "&access_token=" + c.accessToken + "&language=" + c.language);
                    e.get(r, function(a) {
                        t.addClass("mbaa-address-autocomplete"), s.find("li.mbaa").remove(), s.addClass("mbaa-fill"), a.features && (d = a.features, e.each(d, function(e, t) {
                            s.append('<li data-id="' + t.id + '" class="mbaa">' + t.place_name + "</li>")
                        }), s.find("li").each(function(a, n) {
                            e(this).on("click", function() {
                                var a = e(this).data("id");
                                e.each(d, function(n, i) {
                                    if (i.id === a) {
                                        var d = o(i);
                                        e('input[name="mbaa-found-address"]').val(JSON.stringify(d)), s.removeClass("mbaa-fill"), t.val(d.formatted_address).removeClass("mbaa-address-autocomplete").trigger("mapboxAutocomplete.found.address", [d, i])
                                    }
                                })
                            })
                        }))
                    })
                }
                var l, c
            })
        }
    }, e.fn[t] = function(a) {
        var s = e.makeArray(arguments).slice(1);
        return this.each(function() {
            var i = e.data(this, t);
            if (!i) {
                var d = new n(this, a);
                return e.data(this, t, d), d
            }
            i[a] ? i[a].apply(i, s) : e.error("Method " + a + " does not exist on Plugin ?")
        })
    }
}(jQuery);
