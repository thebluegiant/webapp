using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Blitzmove.Data;
using Blitzmove.Hubs;
using Blitzmove.Models;
using Blitzmove.Services;

namespace Blitzmove
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // The Tempdata provider cookie is not essential. Make it essential
            // so Tempdata is functional when tracking is disabled.
            services.Configure<CookieTempDataProviderOptions>(options => {
                options.Cookie.IsEssential = true;
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // CONNECTION STRING SECTION

            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            }
            else // Use SQL Database if in Azure
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
                // Automatically perform database migration
                services.BuildServiceProvider().GetService<ApplicationDbContext>().Database.Migrate();
            }
            // END CONNECTION STRING SECTION

            services.AddDefaultIdentity<ApplicationUser>()
//             // to have control over UI, don't use AddDefaultUI https://docs.microsoft.com/en-us/aspnet/core/security/authentication/scaffold-identity?view=aspnetcore-2.2&tabs=visual-studio
//                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddAuthentication()
                    .AddCookie()
                    .AddGoogle(googleOptions =>  
                    {  
                        googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];  
                        googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];  
                    }).AddFacebook(facebookOptions =>
                    {
                        facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                        facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                    });

            services.AddMvc(option => option.EnableEndpointRouting = false)
                    .AddRazorPagesOptions(options => {
                        options.Conventions.AuthorizePage("/CreatePlace")
                                           .AuthorizePage("/Apply")
                                           .AuthorizePage("/MyPlaces")
                                           .AuthorizePage("/MyApplications")
                                           .AuthorizePage("/Applicants")
                                           .AuthorizePage("/Favorites")
                                           .AuthorizePage("/Messages");
                    })
                    .SetCompatibilityVersion(CompatibilityVersion.Latest);
            
            services.AddScoped<IAdsService, AdsService>();
            services.AddScoped<IApplicationsService, ApplicationsService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddSignalR();

            services.AddTransient<ICustomEmailSender, CustomEmailSender>();
            services.AddTransient<GoogleReCaptchaService>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 7;
            });

            // email confirmation required
            services.Configure<IdentityOptions>(options =>
            {
                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = false;
            });

            services.Configure<ReCAPTCHASettings>(Configuration.GetSection("GooglereCAPTCHA"));


            // services.AddAntiforgery(options => 
            // {
            //     //if we set the header name ajax won't work by default
            //     options.HeaderName = "X-XSRF-TOKEN-BLITZMOVE";
            // });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            // app.UseAntiforgeryToken(); //todo

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            // Always call UseAuthentication before UseSignalR so that SignalR has a user on the HttpContext.
            app.UseAuthentication();
            app.UseSignalR(routes =>
            {
                routes.MapHub<MessageHub>("/chatHub");
            });
            
            app.UseMvcWithDefaultRoute();
            //app.UseMvc();
        }
    }
}
