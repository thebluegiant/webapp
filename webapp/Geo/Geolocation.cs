using System;
using System.Linq;
// using System.Runtime.CompilerServices; 


namespace Blitzmove.Geo
{
    public static class Utils
    {
        // [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool PlaceIsInBounds(double pt_lng, double pt_lat, 
                                            double ne_lng, double ne_lat, double sw_lng, double sw_lat)
        {
            var lng = (pt_lng - ne_lng) * (pt_lng - sw_lng) < 0;
            var lat = (pt_lat - ne_lat) * (pt_lat - sw_lat) < 0;
            return lng && lat;
        }
    }

    public class Coordinates
    {
        public Coordinates() {}
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Coordinates(double longitude, double latitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }

    public static class CoordinatesDistanceExtensions
    {
        public static double DistanceTo(this Coordinates baseCoordinates, Coordinates targetCoordinates)
        {
            return DistanceTo(baseCoordinates, targetCoordinates, UnitOfLength.Kilometers);
        }

        public static double DistanceTo(this Coordinates baseCoordinates, Coordinates targetCoordinates, UnitOfLength unitOfLength)
        {
            var baseRad = Math.PI * baseCoordinates.Latitude / 180;
            var targetRad = Math.PI * targetCoordinates.Latitude / 180;
            var theta = baseCoordinates.Longitude - targetCoordinates.Longitude;
            var thetaRad = Math.PI * theta / 180;

            double dist = Math.Sin(baseRad) * Math.Sin(targetRad) + Math.Cos(baseRad) *
                          Math.Cos(targetRad) * Math.Cos(thetaRad);
            dist = Math.Acos(dist);

            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            return unitOfLength.ConvertFromMiles(dist);
        }
    }

    public class UnitOfLength
    {
        public static UnitOfLength Kilometers = new UnitOfLength(1.609344);
        public static UnitOfLength NauticalMiles = new UnitOfLength(0.8684);
        public static UnitOfLength Miles = new UnitOfLength(1);

        private readonly double _fromMilesFactor;

        private UnitOfLength(double fromMilesFactor)
        {
            _fromMilesFactor = fromMilesFactor;
        }

        public double ConvertFromMiles(double input)
        {
            return input*_fromMilesFactor;
        }
    }

    public class CoordinateGenerator
    {
        public Coordinates[] Calculate(int size, Coordinates location1, Coordinates location2, Coordinates location3,
        Coordinates location4)
        {
            Coordinates[] allCoords = {location1, location2, location3, location4};
            double minLat = allCoords.Min(x => x.Latitude);
            double minLon = allCoords.Min(x => x.Longitude);
            double maxLat = allCoords.Max(x => x.Latitude);
            double maxLon = allCoords.Max(x => x.Longitude);

            Random r = new Random();

            Coordinates[] result = new Coordinates[size];
            for (int i = 0; i < result.Length; i++)
            {
                Coordinates point = new Coordinates();
                do
                {
                    point.Latitude = r.NextDouble()*(maxLat - minLat) + minLat;
                    point.Longitude = r.NextDouble()*(maxLon - minLon) + minLon;
                } while (!IsPointInPolygon(point, allCoords));
                result[i] = point;
            }

            return result;
        }

        //took it from http://codereview.stackexchange.com/a/108903
        //you can use your own one
        private bool IsPointInPolygon(Coordinates point, Coordinates[] polygon)
        {
            int polygonLength = polygon.Length, i = 0;
            bool inside = false;
            // x, y for tested point.
            double pointX = point.Longitude, pointY = point.Latitude;
            // start / end point for the current polygon segment.
            double startX, startY, endX, endY;
            Coordinates endPoint = polygon[polygonLength - 1];
            endX = endPoint.Longitude;
            endY = endPoint.Latitude;
            while (i < polygonLength)
            {
                startX = endX;
                startY = endY;
                endPoint = polygon[i++];
                endX = endPoint.Longitude;
                endY = endPoint.Latitude;
                //
                inside ^= ((endY > pointY) ^ (startY > pointY)) /* ? pointY inside [startY;endY] segment ? */
                        && /* if so, test if it is under the segment */
                        (pointX - endX < (pointY - endY)*(startX - endX)/(startY - endY));
            }
            return inside;
        }
    }

}