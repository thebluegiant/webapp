using Blitzmove.Models;
using Blitzmove.Geo;



namespace Blitzmove.Algorithms
{
    public static class Matching
    {
        public static bool PlacesMatches(Place p1, Place p2, int maxDistance)
        {
            var distance1 = new Coordinates(p1.GpsLongitude.Value, p1.GpsLatitude.Value)
                                .DistanceTo(new Coordinates(p2.NewGpsLongitude.Value, p2.NewGpsLatitude.Value), UnitOfLength.Kilometers);

            var distance2 = new Coordinates(p2.GpsLongitude.Value, p2.GpsLatitude.Value)
                                .DistanceTo(new Coordinates(p1.NewGpsLongitude.Value, p1.NewGpsLatitude.Value), UnitOfLength.Kilometers);

            return (distance1 <= maxDistance && distance2 <= maxDistance)  ? true : false;
        }
    } 
    
}
        
