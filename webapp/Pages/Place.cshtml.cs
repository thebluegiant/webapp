using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;


namespace Blitzmove.Pages
{
    public class PlaceModel : PageModel
    {
        private readonly IAdsService _adsService;
        private readonly IApplicationsService _appsService;
        private readonly IMessageService _messagesService;

        private readonly UserManager<ApplicationUser> _userManager;

        public ApplicationUser AppUser { get; private set; }

        public const string MessageKey = nameof(MessageKey);
        public const string MessageSent = nameof(MessageSent);
        public const string MessageOwn = nameof(MessageOwn);

        public Place Place { get; private set; }

        public string ProfilePicture {get; set;}
        public string ProfileName {get; set;}
        public bool IsFaved { get; private set; }
        public bool UserIsOwner { get; private set; } = false;

        [BindProperty]
        public int RewardValue { get; set; }

        [BindProperty]
        public string Message { get; set; }

        public PlaceModel(UserManager<ApplicationUser> userManager, 
                          IAdsService adsService, IApplicationsService appsService, IMessageService messagesService)
        {
            _userManager = userManager;
            _adsService = adsService;
            _appsService = appsService;
            _messagesService = messagesService;
        }

        public async Task OnGetAsync()
        {
            var id = Guid.Parse(RouteData.Values["id"].ToString());
            Place = await _adsService.FindAsync(id);
            AppUser = await _userManager.GetUserAsync(User);

            // profile picture and name 
            ApplicationUser PlaceUser = await _userManager.FindByIdAsync(Place.UserId);
            if (PlaceUser != null)
            {
                ProfilePicture = PlaceUser.ProfilePicture;
                ProfileName = PlaceUser.Name;

                // checking if current user is the owner
                if (AppUser != null)
                {
                    if(AppUser.Id == PlaceUser.Id)
                    {
                        UserIsOwner = true;
                    }
                }
            }

            // checking if current user has already applied or not
            if(AppUser != null)
            {
                bool alreadyApplied = await _appsService.UserHasAlreadyAppliedAsync(Guid.Parse(AppUser.Id), id);
                if(alreadyApplied) TempData[MessageKey] = "You have already applied to this place.";
            }

            // incrementing visitor count
            bool incremented = false;
            if(AppUser != null)
            {
                //when the visitor is not the poster
                if(AppUser.Id != Place.UserId)
                {
                    Place.VisitorCount += 1;
                    incremented = true;
                }
            }
            else // unregistered users
            {
                Place.VisitorCount += 1;
                incremented = true;
            }
            
            if(incremented) await _adsService.UpdateAsync(Place);

            // checking if user has favorited this place
             if(User.Identity.IsAuthenticated)
            {
                var favList = await _adsService.FindUserFavoritesAsync(Guid.Parse(AppUser.Id));
                if (favList != null) IsFaved = favList.PlaceIds.Any(item => item.PlaceId == Place.Id);
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var placeId = RouteData.Values["id"];
            
            if(User.Identity.IsAuthenticated)
            {
                AppUser = await _userManager.GetUserAsync(User);
                var userId = Guid.Parse(AppUser.Id);

                var place = await _adsService.FindAsync(Guid.Parse(Convert.ToString(placeId)));

                if(place.UserId == AppUser.Id)
                {
                    TempData[MessageOwn] = "You cannot apply to your own place.";
                    return RedirectToAction(Request.Path);
                }
            }

            return RedirectToPage("/Apply", new {placeId, rewardValue = RewardValue});
        }

        public async Task<IActionResult> OnPostMessageAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            AppUser = await _userManager.GetUserAsync(User);
            var id = Guid.Parse(RouteData.Values["id"].ToString());
            Place = await _adsService.FindAsync(id);

            if(AppUser == null) {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            var senderId = AppUser.Id;
            var recipientId = Place.UserId;
            if (!string.IsNullOrEmpty(Message))
            {
                await _messagesService.SaveMessage(senderId, recipientId, Message);
                TempData[MessageSent] = "Message sent successfully.";
            }

            return RedirectToAction(Request.Path);
        }

        public async Task<IActionResult> OnPostAddFavoriteAsync(string favid)
        {
            AppUser = await _userManager.GetUserAsync(User);
            var userId = Guid.Parse(AppUser.Id);

            var favId = Guid.Parse(favid);
            
            var favList = await _adsService.FindUserFavoritesAsync(userId) ?? new UserFavorites();
            
            // prevents adding twice a fav
            if (favList.PlaceIds.Any(x => x.PlaceId == favId)) return Page();
            
            favList.PlaceIds.Add(new Favorite{PlaceId = favId}); 

            if (favList.UserId == Guid.Empty) // if the UserFavorites is new
            {
                favList.UserId = userId;
                await _adsService.AddUserFavoritesAsync(favList);
            }
            else await _adsService.UpdateUserFavoritesAsync(favList);

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostRemoveFavoriteAsync(string favid)
        {
            AppUser = await _userManager.GetUserAsync(User);
            var userId = Guid.Parse(AppUser.Id);

            var favId = Guid.Parse(favid);

            var favList = await _adsService.FindUserFavoritesAsync(userId);
            
            // just in case
            if (!favList.PlaceIds.Any(x => x.PlaceId == favId)) return Page(); 
            
            favList.PlaceIds.Remove(favList.PlaceIds.Where(a => a.PlaceId == favId).First());

            await _adsService.UpdateUserFavoritesAsync(favList);

            return RedirectToPage();
        }

    }
}