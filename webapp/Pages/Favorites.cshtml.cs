using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;


namespace Blitzmove.Pages
{
    public class FavoritesModel : PageModel
    {
        private readonly IAdsService adsService;

        private UserManager<ApplicationUser> _userManager;

        private UserFavorites FavList;
        public ApplicationUser appUser { get; private set; }

        public Place[] MyFavorites { get; set; } = new Place[0];

        public FavoritesModel(UserManager<ApplicationUser> userManager, IAdsService adsService)
        {
            _userManager = userManager;
            this.adsService = adsService;
        }

        public async Task OnGetAsync()
        {
            appUser = await _userManager.GetUserAsync(User);
            var userid = Guid.Parse(appUser.Id);
            FavList = await adsService.FindUserFavoritesAsync(userid);
            if(FavList == null) return;


            MyFavorites = new Place[FavList.PlaceIds.Count];
            for(int i = 0; i < FavList.PlaceIds.Count; i++)
            {
                var place = await adsService.FindAsync(FavList.PlaceIds[i].PlaceId);
                if (place != null) MyFavorites[i] = place;
                else
                {
                    // in case the place has been deleted by the owner, we remove it from the user favorites
                    FavList.PlaceIds.Remove(
                        FavList.PlaceIds.Where(a => a.PlaceId == FavList.PlaceIds[i].PlaceId).First());

                    await adsService.UpdateUserFavoritesAsync(FavList);
                }
            }
        }

        public async Task<IActionResult> OnPostDeleteAsync(Guid fav)
        {
            appUser = await _userManager.GetUserAsync(User);
            var userid = Guid.Parse(appUser.Id);
            FavList = await adsService.FindUserFavoritesAsync(userid);
            
            FavList.PlaceIds.Remove(FavList.PlaceIds.Where(a => a.PlaceId == fav).First());
            await adsService.UpdateUserFavoritesAsync(FavList);
            return RedirectToPage();
        }
    }
}