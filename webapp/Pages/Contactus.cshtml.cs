using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Blitzmove.Models;
using Blitzmove.Services;
using System.ComponentModel.DataAnnotations;



namespace Blitzmove.Pages
{
    [AllowAnonymous]
    public class ContactusModel : PageModel
    {
        private readonly ICustomEmailSender _emailSender;
        private readonly GoogleReCaptchaService _googleReCaptchaService;


        [BindProperty]
        public ContactFormModel Data { get; set; }

        public const string MessageSent = nameof(MessageSent);


        public class ContactFormModel
        {
            [Required]
            public string Name {get; set;}
            
            [Required]
            [EmailAddress]
            public string Email {get; set;}

            [Required]
            public string Content {get; set;}

            [Required]
            public string Token {get; set;}
        } 

        public ContactusModel(ICustomEmailSender emailSender, GoogleReCaptchaService googleReCaptchaService)
        {
            _emailSender = emailSender;
            _googleReCaptchaService = googleReCaptchaService;
        }

        public async Task<IActionResult> OnPostAsync()
        {   
            // google recaptcha
            var googleRecaptcha = _googleReCaptchaService.VerifyToken(Data.Token);

            if(!googleRecaptcha.Result.success)
            {
                ModelState.AddModelError(string.Empty, "Connection error");
                return Page();
            }
            else if(googleRecaptcha.Result.score <= 0.5)
            {
                ModelState.AddModelError(string.Empty, "Looks like you are not human...");
                return Page();
            }

            if(!ModelState.IsValid) return Page();

            string data = "Name:<br>" + Data.Name + "<br><br>Email:<br>"+ Data.Email + "<br><br>Message:<br>" + Data.Content;
            _emailSender.ConfigureSender("pickamove");
            await _emailSender.SendEmailAsync("pickamove@zohomail.eu", "Contact Us: New Message", data);

            TempData[MessageSent] = "Message sent successfully.";
            return RedirectToAction(Request.Path);
        }
    }
}