﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Blitzmove.Pages
{
    public class ApplyModel : PageModel
    {
        private readonly IAdsService _adsService;
        private readonly IApplicationsService _appsService;
        private readonly IMessageService _msgsService;
        private readonly UserManager<ApplicationUser> _userManager;


        public Place Place { get; set; }

        [BindProperty]
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [BindProperty]
        [Display(Name = "Yearly Salary")]
        public int Salary { get; set; }

        [BindProperty]
        [Display(Name = "Current credit score")]
        public int CreditScore { get; set; }

        [BindProperty]
        public int RewardValue { get; set; }

        [BindProperty]
        public string Details { get; set; }

        public ApplicationUser AppUser { get; private set; }

        public ApplicationToAnAd ApplicationToAnAd { get; set; }

        public const string MessageKey = nameof(MessageKey);
        public const string MessageOwn = nameof(MessageOwn);

        public Guid SuperContactId {get; private set;} =  Guid.Parse("00000000-0000-0000-0000-000000000777");

        public ApplyModel(UserManager<ApplicationUser> userManager, IAdsService adsService, 
                          IApplicationsService appsService, IMessageService msgsService)
        {
            _userManager = userManager; // HttpContext is null in page ctor, use elsewhere
            _adsService  = adsService;
            _appsService = appsService;
            _msgsService = msgsService;
        }

        public async Task<IActionResult> OnGetAsync(Guid placeId, int rewardValue)
        {
            AppUser = await _userManager.GetUserAsync(User);
            if (AppUser == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            Place = await _adsService.FindAsync(placeId); // todo if not found
            RewardValue = rewardValue;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            AppUser = await _userManager.GetUserAsync(User);
            var placeId = RouteData.Values["placeID"].ToString();
            var placeGuid = Guid.Parse(placeId);
            var place = await _adsService.FindAsync(placeGuid);

            // forbidding applying to own place
            var userId = Guid.Parse(AppUser.Id);
            if(place.UserId == AppUser.Id)
            {
                TempData[MessageOwn] = "You cannot apply to your own place.";
                return RedirectToAction(Request.Path);
            }

            // if already applied, do nothing
            var alreadyApplied = await _appsService.UserHasAlreadyAppliedAsync(Guid.Parse(AppUser.Id), placeGuid);
            if(alreadyApplied) 
            {
                TempData[MessageKey] = "You have already applied to this place. In case you want to change it, please delete it first on My Applications.";
                return RedirectToAction(Request.Path);
            }
            
            // if not, continue
            // updating applicant profile if 3 fields are filled
            if (CreditScore != 0 || Salary != 0 || !string.IsNullOrEmpty(JobTitle) )
            {
                AppUser.CreditScore = CreditScore;
                AppUser.Salary = Salary;
                AppUser.JobTitle = JobTitle ?? string.Empty;
            }

            ApplicationToAnAd = new ApplicationToAnAd
            {
                Applicant = AppUser,
                PlaceAppliedToGuid = placeGuid,
                PromisedReward = RewardValue,
                Details = Details
            };


            // Sending message to place poster
            var recipient = await _userManager.FindByIdAsync(place.UserId);
            string recipientName = recipient.Name;
            string msg = "Hi " + recipientName + ", I have just applied to your place in " + place.Address + ". Best Regards, " + AppUser.Name;
            await _msgsService.SaveMessageAndEmail(AppUser.Id, place.UserId, msg, msg);

            // Sending message to current top applicant(s) if outbidded
            var currentMaxReward = place.Rewards.Count() != 0 ? place.Rewards.Max(x => x.Value) : 0;
            if(RewardValue >= currentMaxReward)
            {
                var topApplicants = place.Rewards.Where(r => r.Value == currentMaxReward).Select(c => c.ApplicantId).ToList();
                foreach (var top in topApplicants)
                {
                    recipient = await _userManager.FindByIdAsync(top.ToString());
                    recipientName = recipient.Name;
                    msg = "Hi " + recipientName + "," + Environment.NewLine + "Someone has just offered a greater reward for " + place.Address + "." + Environment.NewLine +
                          "If still interested, you can change your application by deleting it and re-applying again with a greater reward.";
 
                    await _msgsService.SaveMessage(SuperContactId.ToString(), top.ToString(), msg);
                }
            }
            

            // updating number of applicants and reward list for the place
            place.NumberOfApplicants += 1;

            if(RewardValue != 0) {
                Reward r = new Reward {ApplicantId = Guid.Parse(AppUser.Id), Value = RewardValue};
                place.Rewards.Add(r);
            }

            await _adsService.UpdateAsync(place);
            await _appsService.AddAndSave(ApplicationToAnAd);

            TempData[MessageKey] = "Current tenant will evaluate your application before scheduling a visit.";
            return RedirectToAction(Request.Path); // redirect to the GET
        }
    }
}