using System;
using System.Threading.Tasks;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Blitzmove.Pages
{
    public class MyApplicationsModel : PageModel
    {
        public class OneApplication
        {
            public ApplicationToAnAd Application { get; set; }
            public string Address { get; set;}
        }

        private readonly IAdsService _adsService;
        private readonly IApplicationsService _appsService;


        private UserManager<ApplicationUser> _userManager;

        private ApplicationToAnAd[] _myApplications { get; set; }

        public OneApplication[] MyApplications { get; set; }

        public ApplicationUser appUser { get; private set; }

        public MyApplicationsModel(UserManager<ApplicationUser> userManager, IAdsService adsService, IApplicationsService appsService)
        {
            _userManager = userManager;
            _adsService = adsService;
            _appsService = appsService;
        }

        public async Task OnGetAsync()
        {
            appUser = await _userManager.GetUserAsync(User);
            _myApplications =  await _appsService.FindByUserAsync(appUser.Id);

            // filling MyApplications
            MyApplications = new OneApplication[_myApplications.Length];
            for(int i = 0; i < _myApplications.Length; i++)
            {
                Place place =  await _adsService.FindAsync(_myApplications[i].PlaceAppliedToGuid);

                if(place != null) 
                    MyApplications[i] = new OneApplication{Application = _myApplications[i], Address = place.Address};
                else MyApplications[i] = new OneApplication{Application = _myApplications[i], Address = "deleted"};
            }
        }

        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            await _appsService.DeleteAsync(id);
            return RedirectToPage();
        }
    }
}