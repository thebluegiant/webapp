using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Blitzmove.Models;
using Blitzmove.Data;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

using Blitzmove.Services;
using System.Text.Encodings.Web;


namespace Blitzmove.Pages
{
    [RequestSizeLimit(100000000)] // by default it's 30mb
    public class CreatePlaceModel : PageModel
    {
        private readonly IAdsService _adsService;
        private readonly ICustomEmailSender _emailSender;
        private readonly GoogleReCaptchaService _googleReCaptchaService;


        private UserManager<ApplicationUser> _userManager;
        public ApplicationUser AppUser { get; private set; }


        [FromRoute]
        public Guid Id { get; set; }

        public bool IsNewPlace => Id.Equals(Guid.Empty);

        [BindProperty]
        public Place Place { get; set; }

        [BindProperty]
        [DataType(DataType.Upload)]
        [MaxFileSize(11*1024*1024)]
        [AllowedExtensions(new string[] {".jpg", ".png", ".jpeg"})]
        [MaxLength(7, ErrorMessage = "You cannot upload more than 7 pictures")]
        public List<IFormFile> PictureFiles { get; set; }

        public const string MessageKey = nameof(MessageKey);
        public const string MessageId = nameof(MessageId);

        [Required]
        [BindProperty]
        public string GToken {get; set;}


        public CreatePlaceModel(UserManager<ApplicationUser> userManager, IAdsService adsService, ICustomEmailSender emailSender,
                                GoogleReCaptchaService googleReCaptchaService)
        {
            _userManager = userManager;
            _adsService  = adsService;
            _emailSender  = emailSender;
            _googleReCaptchaService = googleReCaptchaService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            Place = await _adsService.FindAsync(Id)  ?? new Place();

            // Users not owning the place cannot edit the place
            AppUser = await _userManager.GetUserAsync(User);
            if(!IsNewPlace && AppUser.Id != Place.UserId) return RedirectToPage("/CreatePlace", new { id = "" });

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            // google recaptcha
            var googleRecaptcha = _googleReCaptchaService.VerifyToken(GToken);

            if(!googleRecaptcha.Result.success)
            {
                ModelState.AddModelError(string.Empty, "Connection error...");
                return Page();
            }
            if(googleRecaptcha.Result.score <= 0.5)
            {
                ModelState.AddModelError(string.Empty, "Looks like you are not human...");
                return Page();
            }


            if(!ModelState.IsValid) {
                return Page();
            }

            var place = await _adsService.FindAsync(Id) ?? new Place();
            AppUser = await _userManager.GetUserAsync(User);

            if(IsNewPlace) place.UserId = AppUser.Id;            


            place.NewAddress = Place.NewAddress;
            place.NewGpsLatitude = Place.NewGpsLatitude;
            place.NewGpsLongitude = Place.NewGpsLongitude;
            place.Rent = Place.Rent;
            place.Bedrooms = Place.Bedrooms;
            place.Address = Place.Address;
            place.GpsLatitude = Place.GpsLatitude;
            place.GpsLongitude = Place.GpsLongitude;
            place.Description = Place.Description;

            place.PostDate = DateTime.Now;
            place.VisitorCount = 0;
            place.NumberOfApplicants = IsNewPlace ? 0 : place.NumberOfApplicants;

            if(this.PictureFiles.Count != 0) place.Pictures.Clear();

            var i = 0;

            foreach (var pic in this.PictureFiles)
            {
                if (pic == null) continue;
                using(var stream = new System.IO.MemoryStream())
                {
                    await pic.CopyToAsync(stream);
                    place.Pictures.Add(new Picture
                    {
                        Data = Transform(stream.ToArray()),
                        ContentType = pic.ContentType,
                        PicOrder = i
                    });
                    i++;
                }
            }
 
            if(IsNewPlace) await _adsService.SaveAsync(place);
            else await _adsService.UpdateAsync(place); // in edit mode

            // Sending congrats email for adding place
            string callbackUrl = Url.Page("/Place", pageHandler: null, values: new { id=place.Id }, protocol: Request.Scheme);
            string title = "Congratulations !";
            string htmlmsg = $"<h2 style='font-weight:100;'> Congratulations {HtmlEncoder.Default.Encode(char.ToUpper(AppUser.Name[0]) + AppUser.Name.Substring(1))} ! </h2> <br><br><br><br>" +
                            "Your place is online. <br><br>" +
                            "You will get notified as soon as people start applying. <br><br>" +
                            $"<div style='text-align:center;'> <a style='text-decoration: none;padding-top:10px;padding-bottom:10px;padding-right:20px;padding-left:20px;border-radius:50px!important;background:#ff8138;color:white;' href='{HtmlEncoder.Default.Encode(callbackUrl)}'><b>VIEW MY PLACE</b></a></div> <br><br><br>" +
                            "Thanks for using Pickamove !";
            _emailSender.ConfigureSender("Pickamove");
            await _emailSender.SendEmailAsync(AppUser.Email, title, htmlmsg);


            TempData[MessageKey] = "This site is new, please share with the people who may be interested.";
            TempData[MessageId] = place.Id.ToString();

            return RedirectToAction(Request.Path); // redirect to the GET
        }

        private byte[] Transform(byte[] imageInBytes)
        {
            using (var image = Image.Load(imageInBytes, out var imageFormat ))
            {
                image.Mutate(x => x.AutoOrient());
                using (var ms = new System.IO.MemoryStream())
                {
                    image.Save(ms, imageFormat);
                    return ms.ToArray();
                }
            }
        }

    }
    
}
