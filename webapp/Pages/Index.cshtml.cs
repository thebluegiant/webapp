﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;

using System.Diagnostics;

using Newtonsoft.Json.Linq;


namespace Blitzmove.Pages
{
    public class ShowedPlace
    {
        public Place place { get; set; }
        public bool isFaved { get; set; } = false;
    }

    [IgnoreAntiforgeryToken(Order = 1001)] // Disables token verification
    public class IndexModel : PageModel
    {
        private readonly IAdsService adsService;

        private UserManager<ApplicationUser> _userManager;

        public Guid  userId { get; private set; }

        public ApplicationUser appUser { get; private set; }

        public ShowedPlace[] selectedPlaces { get; set; }

        public IndexModel(UserManager<ApplicationUser> userManager, IAdsService adsService)
        {
            _userManager = userManager;
            this.adsService = adsService;
        }

        public void OnGet()
        {
            
        }

        public async Task<IActionResult> OnPostAreaAsync(string lookuparea)
        {
            if(User.Identity.IsAuthenticated)
            {
                appUser = await _userManager.GetUserAsync(User);
                userId = Guid.Parse(appUser.Id);
            }

            var resource = JObject.Parse(lookuparea);

            double sw_lng=0, sw_lat=0, ne_lng=0, ne_lat=0 ; 
            /* nw_lng=0, nw_lat=0,  se_lng=0, se_lat=0 */

            foreach (var property in resource.Properties())
            {
                if (property.Name == "nebb")
                {
                    ne_lng = property.Value[0].ToObject<double>();
                    ne_lat = property.Value[1].ToObject<double>();
                }
                else if (property.Name == "swbb")
                {
                    sw_lng = property.Value[0].ToObject<double>();
                    sw_lat = property.Value[1].ToObject<double>();
                }
            }

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var areaPlaces = await adsService.GetPlacesWithinAreaAsync(ne_lng, ne_lat, sw_lng, sw_lat);
           
            // figuring out which places are marked as user's favorites
            selectedPlaces = new ShowedPlace[areaPlaces.Count()];
            for(int i = 0; i < selectedPlaces.Length; i++)
            {
                selectedPlaces[i] = new ShowedPlace();

                selectedPlaces[i].place = areaPlaces[i];
                
                if(User.Identity.IsAuthenticated)
                {
                    var favList = await adsService.FindUserFavoritesAsync(userId);
                    if (favList != null) selectedPlaces[i].isFaved = favList.PlaceIds.Any(item => item.PlaceId == areaPlaces[i].Id);
                }
            }

             stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds/10);
            //Console.WriteLine("RunTime GetPlacesWithinAreaAsync GetPlacesWithinAreaAsync GetPlacesWithinAreaAsync = " + elapsedTime);

            
            //return Content(JsonConvert.SerializeObject(places)); // ContentResult 
            return Partial("_PartialList", this);
        }

        public async Task<IActionResult> OnPostAddFavoriteAsync(string favid)
        {
            appUser = await _userManager.GetUserAsync(User);
            userId = Guid.Parse(appUser.Id);

            var favId = Guid.Parse(favid);
            
            var favList = await adsService.FindUserFavoritesAsync(userId) ?? new UserFavorites();
                        
            // prevents adding twice a fav
            if (favList.PlaceIds.Any(x => x.PlaceId == favId)) return Page();
            
            favList.PlaceIds.Add(new Favorite{PlaceId = favId}); 

            if (favList.UserId == Guid.Empty) // if the UserFavorites is new
            {
                favList.UserId = userId;
                await adsService.AddUserFavoritesAsync(favList);
            }
            else await adsService.UpdateUserFavoritesAsync(favList);

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostRemoveFavoriteAsync(string favid)
        {
            appUser = await _userManager.GetUserAsync(User);
            userId = Guid.Parse(appUser.Id);

            var favId = Guid.Parse(favid);

            var favList = await adsService.FindUserFavoritesAsync(userId);
            
            // just in case
            if (!favList.PlaceIds.Any(x => x.PlaceId == favId)) return Page(); 
            
            favList.PlaceIds.Remove(favList.PlaceIds.Where(a => a.PlaceId == favId).First());

            await adsService.UpdateUserFavoritesAsync(favList);
    
            return RedirectToPage();
        }

    }
}
