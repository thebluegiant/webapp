﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Blitzmove.Pages
{
    public class MessagesModel : PageModel
    {
        public class Contact
        {
            public Guid Id { get; set; }
            public string ProfilePic { get; set;}
        }

        private readonly IMessageService _messagesService;

        private readonly UserManager<ApplicationUser> _userManager;

        public ApplicationUser AppUser { get; private set; }
        
        public Dictionary<Guid, Tuple<string, List<Message>>> Conversations { get; private set; }

        public List<Message> CurrentConversationMessages { get; set; }

        public List<Contact> Contacts { get; set; } = new List<Contact>();

        public Guid ActiveConversationContactGuid { get; set; }

        public Guid SuperContactId {get; private set;} =  Guid.Parse("00000000-0000-0000-0000-000000000777");
        
        public string ActiveConversationContactName { get; set; }

        public Guid AppUserId { get; set; }

        [BindProperty]
        public string WrittenMessageFieldContent { get; set; }

        public MessagesModel(UserManager<ApplicationUser> userManager, IMessageService messagesService)
        {
            _userManager = userManager;
            _messagesService = messagesService;
        }

        private async Task InitializeModel()
        {
            AppUser = await _userManager.GetUserAsync(User);
            if (AppUser == null) return;
            AppUserId = Guid.Parse(AppUser.Id);
                        
            Conversations = new Dictionary<Guid, Tuple<string, List<Message>>>();
            var allMessages = await _messagesService.GetAllUserMessages(AppUser.Id);

            var senders = allMessages.Select(m => m.SenderGuid);
            var recipients = allMessages.Select(m => m.RecipientGuid);
            var listOfContactIds = senders.Concat(recipients).Distinct().ToList();

            // FILLING CONTACT LIST
            // 1 - Adding blitzmove as supercontact
            Contact superContact = new Contact{Id = SuperContactId, ProfilePic = null};
            Contacts.Add(superContact);
            // 2 - Adding normal contacts
            foreach(var cid in listOfContactIds)
            {
                var contactUser =  await _userManager.FindByIdAsync(cid.ToString());
                if(contactUser == null) continue;
                Contact elem = new Contact{Id = cid, ProfilePic = contactUser.ProfilePicture};
                Contacts.Add(elem);
            }
            // 3 - Removing ourselves from contact list
            Contacts.RemoveAll(x => x.Id == AppUserId);


            // FILLING DICTIONARY
            foreach (var contact in Contacts)
            {
                var exchangedWithContactOther = allMessages.AsParallel().Where(m => m.RecipientGuid.Equals(contact.Id));
                var exchangedWithContact = allMessages.AsParallel().Where(m => m.RecipientGuid.Equals(contact.Id) || m.SenderGuid.Equals(contact.Id));
                
                Conversations.Add(contact.Id, new Tuple<string, List<Message>>(
                                                await GetUsername(contact.Id),
                                                exchangedWithContact.OrderBy(m => m.CreatedDateTime).ToList()));
            }

            // By default, the 1rst contact is selected. In this case, it's the blitzmove supercontact 
            if(ActiveConversationContactGuid == Guid.Empty)
            {
                ActiveConversationContactGuid = Conversations.FirstOrDefault().Key;
                ActiveConversationContactName = await _messagesService.GetUsername(ActiveConversationContactGuid.ToString());
            }

            // List of messages to load in the partial view 
            CurrentConversationMessages = Conversations.ContainsKey(ActiveConversationContactGuid) 
                ? Conversations[ActiveConversationContactGuid].Item2 : new List<Message>();
        }

        private async Task<string> GetUsername(Guid id)
        {
            if (id == SuperContactId) return "Blitzmove";

            var username = await _messagesService.GetUsername(id.ToString());
            return username;
        }

        
        public async Task OnGetAsync()
        {
            await InitializeModel();
        }


        public async Task<IActionResult> OnGetConvoPartialAsync(string selectedContactId)
        {
            ActiveConversationContactGuid = Guid.Parse(selectedContactId);
            ActiveConversationContactName = await _messagesService.GetUsername(ActiveConversationContactGuid.ToString());
            await InitializeModel();
            return Partial("_ConversationPartial", this);
        }
        
        public IActionResult OnGetMessagePartial(string message, bool isSending, string ownerId)
        {
            var messagePartial = new PartialViewResult
            {
                ViewName = "_MessagePartial",
                ViewData = new ViewDataDictionary
                    <Tuple<string, bool, string>>(ViewData, Tuple.Create(message, isSending, ownerId))
            };

            return messagePartial;
        }

        public async Task<IActionResult> OnPostArchiveConversationAsync(string selectedContact)
        {
            AppUser = await _userManager.GetUserAsync(User);
            AppUserId = Guid.Parse(AppUser.Id);
            Guid contactGuid = Guid.Parse(selectedContact);

            await _messagesService.ArchiveConversation(AppUserId, contactGuid);

            return RedirectToPage();
        }
    }
}