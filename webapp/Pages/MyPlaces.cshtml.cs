using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;  
using Blitzmove.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Blitzmove.Pages
{
    public class MyPlacesModel : PageModel
    {
        public class Applicant
        {
            public Guid Id { get; set; }
            public Guid ApplicationId { get; set; }
            public int ApplicationStatus { get; set; }
            public DateTime Appliedon { get; set; }
            public int PromisedReward { get; set; }
            public int CreditScore { get; set; }
            public int Salary { get; set; }
            public string JobTitle { get; set;}
            public string Username { get; set;}
            public string ProfilePic { get; set;}
            public string Details { get; set;}
        }

        public class MyPlace
        {
            public Place place { get; set;}
            public List<Applicant> ListOfApplicants { get; set; } = new List<Applicant>();
        }

        private readonly IAdsService _adsService;
        private readonly IApplicationsService _appsService;
        private readonly IMessageService _messagesService;
        private UserManager<ApplicationUser> _userManager;

        public ApplicationUser AppUser { get; private set; }

        public MyPlace[] MyPlaces { get; set; }

        [BindProperty]
        public string ApplicantId { get; set; }

        [BindProperty]
        public string Message { get; set; }

        public MyPlacesModel(UserManager<ApplicationUser> userManager, 
                             IAdsService adsService, IApplicationsService appsService, IMessageService messagesService)
        {
            _userManager = userManager;
            _adsService = adsService;
            _appsService = appsService;
            _messagesService = messagesService;
        }

        public async Task OnGetAsync()
        {
            AppUser = await _userManager.GetUserAsync(User);
            var places =  await _adsService.FindByUserAsync(AppUser.Id);

            MyPlaces = new MyPlace[places.Length];

            if(places == null || MyPlaces == null) return;
            
            for(int i = 0; i < places.Length; i++)
            {
                var place = places[i];
                var placeApplications =  await _appsService.FindByPlaceAsync(place.Id);

                // filling MyPlaces
                MyPlaces[i] = new MyPlace();
                MyPlaces[i].place = place;
                if(placeApplications != null && place != null)
                {
                    for(int k = 0; k < placeApplications.Length; k++)
                    {
                        // filling Applicants
                        Applicant item = new Applicant();
                        
                        item.Id = Guid.Parse(placeApplications[k].Applicant.Id);
                        item.ApplicationId = placeApplications[k].Id;
                        item.ApplicationStatus = placeApplications[k].Status;
                        item.Appliedon = placeApplications[k].CreationTime;
                        item.PromisedReward = placeApplications[k].PromisedReward;
                        item.Username = placeApplications[k].Applicant.Name;
                        item.ProfilePic = placeApplications[k].Applicant.ProfilePicture;
                        item.Details = placeApplications[k].Details;
                        item.CreditScore = placeApplications[k].Applicant.CreditScore;
                        item.JobTitle = placeApplications[k].Applicant.JobTitle;
                        item.Salary = placeApplications[k].Applicant.Salary;

                        MyPlaces[i].ListOfApplicants.Add(item);
                    }
                }
            }
        }

        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            await _adsService.DeleteAsync(id);
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostMessageAsync()
        {
            AppUser = await _userManager.GetUserAsync(User);
            await _messagesService.SaveMessage(AppUser.Id, ApplicantId, this.Message);
            return RedirectToPage();
        }


        public async Task<IActionResult> OnPostDeclineAsync(Guid id, Guid query)
        {
            var placeApps =  await _appsService.FindByPlaceAsync(id);
            var place = await _adsService.FindAsync(id);
            var app = Array.Find(placeApps, x => x.Id.Equals(query));

            app.Status = 0;

            await _appsService.UpdateAsync(app);
            await notifyApplicant(place.Address, Guid.Parse(place.UserId) , Guid.Parse(app.Applicant.Id), 0);

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostInviteAsync(Guid id, Guid query)
        {
            var placeApps =  await _appsService.FindByPlaceAsync(id);
            var place = await _adsService.FindAsync(id);
            var app = Array.Find(placeApps, x => x.Id.Equals(query));

            app.Status = 1;

            await _appsService.UpdateAsync(app);
            await notifyApplicant(place.Address, Guid.Parse(place.UserId) , Guid.Parse(app.Applicant.Id), 1);

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostRequestPaymentAsync(Guid id, Guid query)
        {
            var placeApps =  await _appsService.FindByPlaceAsync(id);
            var place = await _adsService.FindAsync(id);
            var app = Array.Find(placeApps, x => x.Id.Equals(query));

            app.Status = 2;
            
            await _appsService.UpdateAsync(app);
            await notifyApplicant(place.Address, Guid.Parse(place.UserId) , Guid.Parse(app.Applicant.Id), 2);

            // disabling applications of the other applicants => Actions can no longer be taken by the poster
            var otherApps = placeApps.Where(p => p.Id != app.Id).ToArray();
            foreach(var other in otherApps)
            {
                other.Status = -2;
            }
            await _appsService.UpdateRangeAsync(otherApps);


            return RedirectToPage();
        }
         
        public async Task<IActionResult> OnPostCancelPaymentRequestAsync(Guid id, Guid query)
        {
            var placeApps =  await _appsService.FindByPlaceAsync(id);
            var place = await _adsService.FindAsync(id);
            var app = Array.Find(placeApps, x => x.Id.Equals(query));

            app.Status = -1; // default: in-progress
            await notifyApplicant(place.Address, Guid.Parse(place.UserId) , Guid.Parse(app.Applicant.Id), -1);

            await _appsService.UpdateAsync(app);

            // re-enabling applications of the other applicants => Actions can be taken by the poster
            var otherApps = placeApps.Where(p => p.Id != app.Id).ToArray();
            foreach(var other in otherApps)
            {
                other.Status = -1;
            }
            await _appsService.UpdateRangeAsync(otherApps);


            return RedirectToPage();
        }

        private async Task notifyApplicant(string placeAddress, Guid posterId, Guid applicantId, int type)
        {
            var poster =  await _userManager.FindByIdAsync(posterId.ToString());
            var applicant =  await _userManager.FindByIdAsync(applicantId.ToString());

            string message = "";

            if(type == 0){
                message += "Hi " + applicant.Name + ", Thank you for applying to my place located in " + placeAddress + 
                           ". Unfortunately, I will have to decline it. Best Regards, " + poster.Name;
            }
            else if(type == 1){
                message += "Hi " + applicant.Name + ", I would like to invite you to visit the place located in " + placeAddress + 
                           ". Let's schedule this ! Best Regards, " + poster.Name;
            }
            else if(type == 2){
                message += "Hi " + applicant.Name + 
                           ", Regarding your application to " + placeAddress + 
                           ", I am requesting the payment for the reward you promised in case of an agreement with the landlord. Best Regards, " + 
                           poster.Name;
            }
            else if(type == -1){
                message += "Sorry " + applicant.Name + ", I am cancelling the payment request I've made you previously. Best Regards, " + poster.Name;
            }
            else return;

            await _messagesService.SaveMessageAndEmail(posterId.ToString(), applicantId.ToString(), message, message);
        }
    }
}
