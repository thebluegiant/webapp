# [Pickamove](https://www.pickamove.com) 

## MVP Todo list 
<details>  
 
### Development
- [x] Good UI in main page for easy and intuitive navigation (in progress 8/10)
- [x] Seed Data for testing (100 ads or more)
- [x] Retrieve Ads from database dynamically by moving the map (C# function taking a bounding box and returning ads array).

- [x] My Favorites using Ajax requests
- [x] My Places Page (For visualizing all my ads and delete/edit them)
- [x] My Applications Page (Applications list | Status {Refused or Accepted, in progress, Completed})

- [ ] Escrow Service (My Balance Page, Choose 3rd-party service manager, etc..)

- [x] Messages (based on https://bootsnipp.com/snippets/1ea0N )

- [x] Make website mobile friendly


### Testing
- [x] Optimize Pipeline (code minification, obfuscation)
- [x] Validation Attributes in Ad.cs for pictures, title, rent, description, etc...


### Deploy
- [x] Let's encrypt: https://www.youtube.com/watch?v=AuMj31MEy5E
- [x] Privacy Section
- [x] Set up Communication with users (phone, email, feedback)
- [x] Set up Analytics


**Start marketing campaign.**
</details>

## PMF Todo list
<details>
 
</details>





